phpcpd 3.0.1 by Sebastian Bergmann.

Found 48 clones with 2237 duplicated lines in 60 files:

  - /var/www/html/gitlab/rosariosis/plugins/Moodle/Scheduling/Courses.php:290-328
    /var/www/html/gitlab/rosariosis/plugins/Moodle/School_Setup/Rollover.php:72-110

  - /var/www/html/gitlab/rosariosis/plugins/Moodle/Students/Student.php:259-377
    /var/www/html/gitlab/rosariosis/plugins/Moodle/Users/User.php:373-491

  - /var/www/html/gitlab/rosariosis/functions/Inputs.php:826-854
    /var/www/html/gitlab/rosariosis/functions/Inputs.php:956-984

  - /var/www/html/gitlab/rosariosis/modules/Accounting/DailyTransactions.php:2-67
    /var/www/html/gitlab/rosariosis/modules/Accounting/DailyTotals.php:9-74

  - /var/www/html/gitlab/rosariosis/modules/Accounting/StaffPayments.php:101-142
    /var/www/html/gitlab/rosariosis/modules/Accounting/Expenses.php:92-133

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/Transactions.php:44-71
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/Accounts.php:70-97

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/MenuReports.php:5-47
    /var/www/html/gitlab/rosariosis/modules/Food_Service/TransactionsReport.php:4-46

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Transactions.php:3-18
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Accounts.php:6-21

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/MenuReports.php:5-44
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Statements.php:4-43

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/Transactions.php:102-161
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Users/Transactions.php:90-149

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/ActivityReport.php:127-158
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Users/ActivityReport.php:111-142

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/ActivityReport.php:163-196
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Users/ActivityReport.php:153-186

  - /var/www/html/gitlab/rosariosis/modules/Food_Service/Students/ServeMenus.php:146-174
    /var/www/html/gitlab/rosariosis/modules/Food_Service/Users/ServeMenus.php:132-160

  - /var/www/html/gitlab/rosariosis/modules/Attendance/DuplicateAttendance.php:271-309
    /var/www/html/gitlab/rosariosis/modules/Attendance/DuplicateAttendance.php:307-345

  - /var/www/html/gitlab/rosariosis/modules/Scheduling/PrintClassPictures.php:231-293
    /var/www/html/gitlab/rosariosis/modules/Scheduling/PrintClassLists.php:118-180

  - /var/www/html/gitlab/rosariosis/modules/Scheduling/Scheduler.php:302-318
    /var/www/html/gitlab/rosariosis/modules/Scheduling/Scheduler.php:370-386

  - /var/www/html/gitlab/rosariosis/modules/Eligibility/Student.php:51-92
    /var/www/html/gitlab/rosariosis/modules/Eligibility/StudentList.php:3-44

  - /var/www/html/gitlab/rosariosis/modules/Eligibility/Student.php:51-100
    /var/www/html/gitlab/rosariosis/modules/Eligibility/TeacherCompletion.php:7-56

  - /var/www/html/gitlab/rosariosis/modules/Scheduling/AddDrop.php:3-41
    /var/www/html/gitlab/rosariosis/modules/Students/AddDrop.php:3-41

  - /var/www/html/gitlab/rosariosis/modules/Students/PeopleFields.php:88-129
    /var/www/html/gitlab/rosariosis/modules/Students/StudentFields.php:118-159

  - /var/www/html/gitlab/rosariosis/modules/Students/PeopleFields.php:9-60
    /var/www/html/gitlab/rosariosis/modules/Students/AddressFields.php:9-60

  - /var/www/html/gitlab/rosariosis/modules/Students/PeopleFields.php:77-129
    /var/www/html/gitlab/rosariosis/modules/Students/AddressFields.php:77-129

  - /var/www/html/gitlab/rosariosis/modules/Grades/includes/HonorRoll.fnc.php:105-162
    /var/www/html/gitlab/rosariosis/modules/Grades/includes/HonorRoll.fnc.php:259-316

  - /var/www/html/gitlab/rosariosis/modules/Grades/Grades.php:760-783
    /var/www/html/gitlab/rosariosis/modules/Grades/Grades.php:803-826

  - /var/www/html/gitlab/rosariosis/modules/Grades/MassCreateAssignments.php:16-53
    /var/www/html/gitlab/rosariosis/modules/Grades/Assignments.php:46-83

  - /var/www/html/gitlab/rosariosis/modules/Grades/GPAMPList.php:2-104
    /var/www/html/gitlab/rosariosis/modules/Grades/GPARankList.php:2-104

  - /var/www/html/gitlab/rosariosis/modules/Library/includes/Library.fnc.php:190-252
    /var/www/html/gitlab/rosariosis/modules/Quiz/includes/Questions.fnc.php:238-300

  - /var/www/html/gitlab/rosariosis/modules/Library/Library.php:92-135
    /var/www/html/gitlab/rosariosis/modules/Quiz/Questions.php:108-151

  - /var/www/html/gitlab/rosariosis/modules/misc/Export.php:158-176
    /var/www/html/gitlab/rosariosis/modules/misc/Export.php:469-487

  - /var/www/html/gitlab/rosariosis/modules/Accounting/StaffPayments.php:46-83
    /var/www/html/gitlab/rosariosis/modules/Student_Billing/StudentPayments.php:44-81

  - /var/www/html/gitlab/rosariosis/modules/Accounting/Salaries.php:41-70
    /var/www/html/gitlab/rosariosis/modules/Student_Billing/StudentFees.php:39-68

  - /var/www/html/gitlab/rosariosis/modules/School_Setup/includes/Modules.inc.php:316-370
    /var/www/html/gitlab/rosariosis/modules/School_Setup/includes/Plugins.inc.php:304-358

  - /var/www/html/gitlab/rosariosis/modules/Students/AssignOtherInfo.php:219-258
    /var/www/html/gitlab/rosariosis/modules/School_Setup/Schools.php:392-431

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:44-88
    /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:102-146

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:103-130
    /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:161-188

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:129-217
    /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:187-275

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:217-283
    /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:280-346

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:282-338
    /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:347-403

  - /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:3-73
    /var/www/html/gitlab/rosariosis/modules/Discipline/CategoryBreakdown.php:3-73

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:129-161
    /var/www/html/gitlab/rosariosis/modules/Discipline/CategoryBreakdown.php:172-204

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:230-295
    /var/www/html/gitlab/rosariosis/modules/Discipline/CategoryBreakdown.php:273-338

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentBreakdown.php:294-338
    /var/www/html/gitlab/rosariosis/modules/Discipline/CategoryBreakdown.php:337-381

  - /var/www/html/gitlab/rosariosis/modules/Discipline/StudentFieldBreakdown.php:3-54
    /var/www/html/gitlab/rosariosis/modules/Discipline/CategoryBreakdownTime.php:3-54

  - /var/www/html/gitlab/rosariosis/modules/Users/Profiles.php:109-147
    /var/www/html/gitlab/rosariosis/modules/Users/Exceptions.php:49-87

  - /var/www/html/gitlab/rosariosis/modules/Users/Profiles.php:478-528
    /var/www/html/gitlab/rosariosis/modules/Users/Exceptions.php:314-364

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentFields.php:9-78
    /var/www/html/gitlab/rosariosis/modules/Users/UserFields.php:9-78

  - /var/www/html/gitlab/rosariosis/modules/Students/StudentFields.php:114-159
    /var/www/html/gitlab/rosariosis/modules/Users/UserFields.php:114-159

  - /var/www/html/gitlab/rosariosis/modules/Students/Search.inc.php:395-434
    /var/www/html/gitlab/rosariosis/modules/Users/Search.inc.php:326-365

2.31% duplicated lines out of 96642 total lines of code.

Time: 1.46 seconds, Memory: 73.00MB
