# RosarioSIS Meta

Tools for RosarioSIS:

- Upgrade
- PHP Quality Assurance (QA)

Place the tools in the `meta/` or `rosariosis-meta/` folder inside RosarioSIS.

## version-upgrade.sh

Interactive script (bash) to replace all occurrences of current version to the new version.

Example: `3.3.2` to `3.4`

Usage:
```
$ chmod +x version-upgrade.sh
$ ./version-upgrade.sh
```

## phpcompatinfo
https://github.com/llaville/php-compat-info
http://php5.laurent-laville.org/compatinfo/manual/5.0/en/user-guide--installation.html

Note: version 5.1.0 requires PHP >= 7.1.
```
$ cd qa
$ wget -O phpcompatinfo.phar http://bartlett.laurent-laville.org/get/phpcompatinfo-5.0.12.phar
$ php phpcompatinfo.phar
$ php phpcompatinfo.phar analyser:run --output=../public/phpcompatinfo.txt --alias rosariosis
```

## PHP Mess Detector
https://github.com/phpmd/phpmd
https://phpmd.org/rules/index.html
```
$ cd qa
$ wget -O phpmd.phar https://static.phpmd.org/php/latest/phpmd.phar
$ php phpmd.phar
$ php phpmd.phar ../../functions/ html codesize > ../public/phpmd_functions_codesize.html
$ php phpmd.phar ../../modules/ html codesize > ../public/phpmd_modules_codesize.html
```


## TODO

- webgrind profiler
https://github.com/jokkedk/webgrind
- PHPDoc
https://github.com/phpDocumentor/phpDocumentor2
- PHPUnit tests
https://github.com/sebastianbergmann/phpunit

See PHP_QA.txt
