#!/bin/bash

printf "Current version: "
read CURRENT_VERSION

printf "New version: "
read VERSION

echo "Upgrading..."

cd ..

cp Warehouse.php Warehouse.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" Warehouse.tmp > Warehouse.php
rm Warehouse.tmp

cp rosariosis.sql rosariosis.tmp
sed "s/'$CURRENT_VERSION'/'$VERSION'/" rosariosis.tmp > rosariosis.sql
rm rosariosis.tmp

cp INSTALL.md INSTALL.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" INSTALL.tmp > INSTALL.md
rm INSTALL.tmp

cp INSTALL_fr.md INSTALL_fr.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" INSTALL_fr.tmp > INSTALL_fr.md
rm INSTALL_fr.tmp

cp INSTALL_es.md INSTALL_es.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" INSTALL_es.tmp > INSTALL_es.md
rm INSTALL_es.tmp

cp COPYRIGHT COPYRIGHT.tmp
sed "s/version $CURRENT_VERSION/version $VERSION/" COPYRIGHT.tmp > COPYRIGHT
rm COPYRIGHT.tmp

# Line 3: upgrade "Month, YYYY" after dash -.
cp COPYRIGHT COPYRIGHT.tmp
sed "3!b;s/- .*/- $(date +%B), $(date +%Y)/" COPYRIGHT.tmp > COPYRIGHT
rm COPYRIGHT.tmp

cd ProgramFunctions/

cp Update.fnc.php Update.fnc.tmp
sed "0,/$CURRENT_VERSION/ s/$CURRENT_VERSION/$VERSION/" Update.fnc.tmp > Update.fnc.php
rm Update.fnc.tmp


echo "Complete!"
